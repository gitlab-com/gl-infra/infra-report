"use strict";

class MergeRequestEvaluator {
  constructor(since, team) {
    this.since = since;
    this.team = team;
    this.list = [];
  }

  visitEventForUser() {}

  visitMergeRequest(mergeRequest) {
    // Was this merged AND authored by someone on the team?
    if (this.team.has(mergeRequest.author.username) && mergeRequest.merged_at && new Date(mergeRequest.merged_at) >= this.since) {
      this.list.push(mergeRequest);
    }
  }

  visitIssue() {}

  render() {
    return this.list.map(mergeRequest => [mergeRequest.author.username, `* @${mergeRequest.author.username}: ${mergeRequest.web_url}: ${mergeRequest.title}`]);
  }
}

module.exports = MergeRequestEvaluator;
