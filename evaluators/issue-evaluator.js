"use strict";

const ISSUE_WEIGHT_EMOJI = "zero|one|two|three|four|five|six|seven|eight|nine|keycap_ten".split("|");

class IssueEvaluator {
  constructor(since, team) {
    this.since = since;
    this.team = team;
    this.issues = [];
  }

  visitEventForUser() {}
  visitMergeRequest() {}
  visitIssue(issue) {
    if (!this.isIssueCreditable(issue)) return;
    this.issues.push(issue);
  }

  render() {
    return this.issues.map(issue => this.renderIssueLine(issue));
  }

  isIssueCreditable(issue) {
    if (issue.confidential) return false;
    if (!issue.closed_at) return false;
    if (new Date(issue.closed_at) < this.since) return false;

    if (issue.assignees.length) {
      return issue.assignees.some(assignee => this.team.has(assignee.username));
    }

    return issue.closed_by && this.team.has(issue.closed_by.username);
  }

  weightAsEmoji(issueWeight) {
    if (!issueWeight) return ":asterisk:";

    let emoji = ISSUE_WEIGHT_EMOJI[issueWeight];
    if (emoji) return `:${emoji}:`;

    return ":arrow_up:";
  }

  renderIssueLine(issue) {
    let assignees = this.attributionForIssue(issue);

    let milestone = issue.milestone ? ` :m: [${issue.milestone.title}](${issue.milestone.web_url})` : "";
    let weight = this.weightAsEmoji(issue.weight);

    return [assignees, `* ${weight} ${assignees}: ${issue.web_url}: ${issue.title}${milestone}`];
  }

  attributionForIssue(issue) {
    let assignees = issue.assignees.map(assignee => `@${assignee.username}`).join(", ");
    if (assignees) return assignees;

    if (issue.closed_by) return `@${issue.closed_by.username}`;

    return "";
  }
}

module.exports = IssueEvaluator;
