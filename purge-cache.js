"use strict";

const Promise = require("bluebird");
const cacache = require("cacache");

const MAX_AGE_DAYS = 3;

async function purgeCache(cachePath = __dirname + "/cache") {
  let entries = await cacache.ls(cachePath);

  let deletions = Object.keys(entries).filter((key) => {
    let entry = entries[key];
    let age = Date.now() - entry.time;
    return age >= MAX_AGE_DAYS * 86400 * 1000;
  });

  if (deletions.length) {
    await Promise.map(deletions, (key) => cacache.rm.entry(cachePath, key), { concurrency: 5 });
  }

  await cacache.verify(cachePath);
}

module.exports = purgeCache;
